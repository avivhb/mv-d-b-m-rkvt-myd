<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Department;
use App\Candidate;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function changeDepartmentUser(Request $request){
        $uid = $request->id;
        $did = $request->department_id;
        if(Gate::allows('user-allow')){
        $user = User::findOrFail($uid);
        $user->department_id = $did;
        $user->save();
        }else{
            Session::flash('messageUserNotAllowed','Since you own a Candidate, your permission to change department is not allowed');  
        }
        return redirect('users');
    }

    public function makemanager($uid){
        if(Gate::allows('make-manager'))
        {
            $user = User::findOrFail($uid);
            $user->department_id ='2';
            $user->save();
            Session::flash('approved','Changed successfully');  
        }
        return redirect('users');
    }


    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.show', compact('user','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('users');
    }
}
