<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use of the Candidate model here - inside the controller
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth; // auth user
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

//full name is "App\Http\Controllers\CandidatesController"
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function changeUser($cid,$uid = null){
        Gate::authorize('assign-user');
        $candidate = Candidate::findOrFail($cid); // Pull the candidate
        $candidate->user_id = $uid; // update the new $uid we recieved from the dropdown.
        $candidate->save();
        return back(); /// back stay in the same page
        //return redirect('candidates'); //return to candidates.index
    }

    // cid - current candidate sid - which status we want to update to//
    public function changeStatus($cid,$sid){
        $candidate = Candidate::findOrFail($cid); // A,B: Pull the candidate
        if(Gate::allows('change-status',$candidate)){
            $from = $candidate->status->id; // B: pull the candidate current status
            if(!Status::allowed($from,$sid)) return redirect('candidates'); //B:
            $candidate->status_id = $sid; // update the new $sid we recieved from the dropdown.
            $candidate->save();
        }else{
            Session::flash('notallowed','You are not allowed to change the status of the user, because you are not the owner of the user');
        }
        return back(); // back stay in the same page
        //return redirect('candidates'); //return to candidates.index
    }

    public function myCandidates() //filter user candidates
    {
        $userId = Auth::id(); //pull the current auth userId
        $user = User::findOrFail($userId); // pull the user row
        $candidates = $user->candidates; // SELECT candidates from USER where user=$user **bring the user candidates**
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses')); // return to the relevant view with all the vars.
    }
    

    public function index()
    {
        $candidates = Candidate::all(); // SELECT * FROM candidates
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses')); // return to the relevant view with all the vars.
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('candidates.create');
    }

    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request is the data we got from the create.blade (@store)
        $candidate = new Candidate(); // create an empty row from the Candidate Model
            //$candidate->name = $request->name;  
            //$candidate->email = $request->email;
            //$candidate->save(); // INSERT into the table
            // mass assing need to have FILLABLE inside candidate model
        $candidate->create($request->all()); //create and not save bacause mass assign
        return redirect('candidates'); //return to candidates.index
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) //get from the url the id of the candidate.
    {
        $candidate = Candidate::findOrFail($id); //pull from the DB the relevant candidate.
        return view('candidates.edit',compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request is all the data we recieve from the PATCH
        // $id is the serial number of the candidate we want to edit

        $candidate = Candidate::findOrFail($id); // pull from the DB the candidate again!
        $candidate->update($request->all());  //Mass assign - update all the feilds that request changed. 
                                             //we must allow this inside the candidates model (fillable)!
        return redirect('candidates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');
    }
}
