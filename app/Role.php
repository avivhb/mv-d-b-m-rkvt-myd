<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){ 
        return $this->belongsToMany('App\User','userroles');
    } // connection between role and user with the table users_roles
}
