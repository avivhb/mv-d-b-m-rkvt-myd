<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //gate name - 'change-status'

        //check if user can change the candidate status (only user that belong to the candidate can do so)
        //
        //the user come automatically in this page bacuse of the GATE!!
        // WE DONT NEED TO SEND IT HERE - IN BOTH FUNCTIONS
        //
        Gate::define('change-status', function ($user,$candidate) {
            return $user->id === $candidate->user_id;
        });

        //check if user can change the user (only manager can)
        Gate::define('assign-user', function($user) {
            return $user->isManager();
        });

        Gate::define('delete-user', function($user) {
            return $user->isAdmin();
        });

        Gate::define('user-allow', function($user) {
            return $user->hasNoCandidate();
        });

        Gate::define('make-manager', function ($user) {
            return $user->isAdmin();
        });



    }
}
