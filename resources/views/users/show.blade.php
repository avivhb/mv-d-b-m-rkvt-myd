@extends('layouts.app')
@section('title','Show')
@section('content');



    <h1>{{$user->name}}</h1>
    <table class="table table-dark">
        <tr>
            <th>Id</th><th>Name</th><th>Email</th><th>Created</th><th>Updated</th><th>isManager</th>
        </tr>
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                @if(!$user->isManager())
                <td>
                <a href ="{{route('users.makemanager',$user->id)}}">update to Manager</a>
                </td>
                @endif
             </tr>
    </table>


    @if(!$user->isAdmin())
           <h2> the department of the user is: {{$user->department->name}} </h2>
    @else
    <form method="POST" action="{{ route('users.changedepartment') }}">
                    @csrf  
                    <div class="form-group row">
                    <label for="department_id" class="col-md-4 col-form-label text-md-right">Choose Department</label>
                    <div class="col-md-6">
                        <select class="form-control" name="department_id">                                                                         
                        @foreach ($departments as $department)
                          <option value="{{ $department->id }}"> 
                              {{ $department->name }} 
                          </option>
                        @endforeach    
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change Department 
                                </button>
                            </div>
                    </div>                    
                </form> 
                </div>
@endif
@endsection;
