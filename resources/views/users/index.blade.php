@extends('layouts.app')
@section('title','Users')
@section('content');

@if(Session::has('messageUserNotAllowed'))
<div class = 'alert alert-danger'>
    {{Session::get('messageUserNotAllowed')}}
</div>
@endif

@if(Session::has('approved'))
<div class = "alert alert-success" role="alert">
    {{Session::get('approved')}}
</div>
@endif

    <h1>List of Users</h1>
    <table class="table table-dark">
        <tr>
            <th>Id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th><th>Updated</th><th>Details</th><th>Delete</th>
        </tr>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                @if(isset($user->department_id))
                            {{$user->department->name}}
                        @else
                            Assign department
                        @endif
                </td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td><a href ="{{route('users.show',$user->id)}}">Details</a></td>
                <td><a href ="{{route('users.delete',$user->id)}}">Delete</a></td>


            </tr>
        @endforeach
    </table>

@endsection;
