@extends('layouts.app')

@section('title','Candidates')

@section('content');

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
    <!-- create the ref that direct us to the create page -->
    <div><a href = "{{url('/candidates/create')}}">Add New Candidate</a></div>

    <h1>List of Candidates</h1>
    <table class="table table-dark">
        <tr>
            <th>Id</th><th>Name</th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
        </tr>
        <!-- loop that generates all the candidates,
            the $candidates came from the controller -->
        @foreach($candidates as $candidate)
            <tr>
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td> <!-- USER DROPDOWN -->
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->user_id))
                            {{$candidate->owner->name}}
                        @else
                            Assign owner
                        @endif
                    
                        </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach($users as $user)
                            <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
                        @endforeach
                    </div>
                    </div>
                </td>
                <td> <!-- STATUS DROPDOWN -->
                    <div class="dropdown">
                    @if(App\Status::next($candidate->status_id) != null) <!-- IF to avoid empty selection -->
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$candidate->status->name}}                
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <!-- we cant have USE on top of the page, instead we call the model function Directly! -->
                        @foreach(App\Status::next($candidate->status_id) as $status)
                            <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach
                    </div>
                    @else
                            {{$candidate->status->name}}   
                    @endif    
                    </div>
                </td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
                <!-- we want to go to the candidate.edit and take with us the id of the candidate -->
                <td><a href ="{{route('candidates.edit',$candidate->id)}}">Edit</a></td>
                <!--href using only the GET Method-->
                <td><a href ="{{route('candidate.delete',$candidate->id)}}">Delete</a></td>
                
            </tr>
        @endforeach
    </table>

@endsection;
