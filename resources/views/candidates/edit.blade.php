@extends('layouts.app')

@section('title','Edit Candidate')

@section('content');

    <h1>Edit Candidate</h1>
    <!-- php artisan route:list -->
    <form method = "post" action ="{{action('CandidatesController@update',$candidate->id)}}">
        @METHOD('PATCH')
        @csrf <!-- Security -->
        <div class="form-group">
            <label for = "name">Candidate Name</label>
            <input type = "text" name = "name" class="form-control" value = {{$candidate->name}}> 
        </div>
        <div class="form-group">
            <label for = "email">Candidate Email</label>
            <input type = "text" name = "email" class="form-control" value = {{$candidate->email}}> 
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Update Candidate">
        </div>
    </form>

@endsection;